pipeline {
  agent any
  
  stages {
   stage('TF Plan') {
       steps {
         container('terraform') {
           sh 'sudo terraform init'
           sh 'sudo terraform plan -out myplan'
         }
       }
     }
   stage('TF Apply') {
      steps {
        container('terraform') {
          sh 'terraform apply -input=false myplan'
        }
      }
    }
  }
}  